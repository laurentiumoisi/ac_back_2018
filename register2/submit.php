<?php

require_once("dbconfig.php");

$firstname = "";
$lastname = "";
$phone = "";
$email = "";
$cnp = "";
$facebook = "";
$birth = "";
$department = "";
$question = "";
$captcha_generated = "";
$captcha_inserted = "";
$check = "";
$facultate="";
$sex="";

if(isset($_POST['firstname'])){
	$firstname = $_POST['firstname'];
}

if(isset($_POST['sex'])){
	$sex = $_POST['sex'];
}

if(isset($_POST['lastname'])){
	$lastname = $_POST['lastname'];
}
if(isset($_POST['facultate'])){
	$facultate = $_POST['facultate'];
}

if(isset($_POST['phone'])){
	$phone = $_POST['phone'];
}

if(isset($_POST['email'])){
	$email = $_POST['email'];
}

if(isset($_POST['cnp'])){
	$cnp = $_POST['cnp'];
}

if(isset($_POST['facebook'])){
	$facebook = $_POST['facebook'];
}

if(isset($_POST['birth'])){
	$birth = $_POST['birth'];
}

if(isset($_POST['department'])){
	$department = $_POST['department'];
}

if(isset($_POST['question'])){
	$question = $_POST['question'];
}

if(isset($_POST['captcha_generated'])){
	$captcha_generated = $_POST['captcha_generated'];
}

if(isset($_POST['captcha_inserted'])){
	$captcha_inserted = $_POST['captcha_inserted'];
}

if(isset($_POST['check'])){
	$check = $_POST['check'];
}
if(empty($firstname) || empty($lastname) || empty($facultate) || empty($phone) || empty($email) || empty($facebook) || empty($cnp) || empty($facebook)|| empty($birth) || empty($department) || empty($question) || empty($captcha_inserted) || empty($check)){
	$error = 1;
	$error_text = "One or more fields are empty!";
}
if((strlen($firstname) < 3 || strlen($lastname) < 3 || strlen($firstname) > 20 || strlen($lastname) > 20) || strlen($question) < 15){
	$error = 1;
	$error_text = "First or Last name or question is shorter/larger than expected!";
}
if(is_numeric($firstname) || is_numeric($lastname)){
	$error = 1;
	$error_text = "First or Last name should not contain numbers";
}
if(is_numeric($facultate) || strlen($facultate) < 3 || strlen($facultate) > 30){
	$error = 1;
	$error_text = "Facultate is not correct";
}

if(!is_numeric($phone) || strlen($phone)!=10){
	$error = 1;
	$error_text = "Phone number is not valid";
}
if (!filter_var($email, FILTER_VALIDATE_EMAIL)){
    $error = 1;
    $error_text = "Email is not correct";
}
if (strlen($cnp)!=13 || preg_match('#^7#',$cnp) == 1 || preg_match('#^8#',$cnp) == 1 || preg_match('#^9#',$cnp) == 1 || preg_match('#^0#',$cnp) == 1){
	$error = 1;
	$error_text = "CNP is not valid";
}

if (preg_match('#^facebook.com/#', $facebook) != 1){
   if(preg_match('#^http://facebook.com/#', $facebook) != 1){
   	   $error = 1;
   	   $error_text = "Facebook is not valid";
}


if($captcha_generated != $captcha_inserted){
	$error = 1;
	$error_text = "Captcha is not correct";
}



try {

    $con = new pdo('mysql:host=' . HOST . ';dbname=' . DATABASE . ';charset=utf8;', USER, PASSWORD);

} catch(Exception $e) {

    $db_error['connection'] = "Cannot connect to database";

    $response = json_encode($db_error);

    header("HTTP/1.1 503 Service Unavailable");

        echo $response;
    return;

}
if ($error == 0){
$stmt2 = $con -> prepare("INSERT INTO register2(firstname,lastname,facultate,phone,email,cnp,facebook,birth,department,question) VALUES(:firstname,:lastname,:facultate,:phone,:email,:cnp,:facebook,:birth,:department,:question)");

$stmt2 -> bindParam(':firstname',$firstname);
$stmt2 -> bindParam(':lastname',$lastname);
$stmt2 -> bindParam(':facultate',$facultate);
$stmt2 -> bindParam(':phone',$phone);
$stmt2 -> bindParam(':email',$email);
$stmt2 -> bindParam(':cnp',$cnp);
$stmt2 -> bindParam(':facebook',$facebook);
$stmt2 -> bindParam(':birth',$birth);
$stmt2 -> bindParam(':department',$department);
$stmt2 -> bindParam(':question',$question);

	if(!$stmt2->execute()){

    $errors['connection'] = "Database Error";

	}
	else{

    	echo "Succes";
 }
}
else {
     echo $error_text;
     return;
}
}
